import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('https://medicallanguagelab.fadavis.com/Home/Main')

WebUI.waitForPageLoad(30)

try {
	WebUI.click(findTestObject('Object Repository/Page_Medical Language Lab/a_ABOUT'))
}
catch (NoSuchElementException e) {
	System.out.println('An exceptional case.')
}

WebUI.waitForPageLoad(20)

try {
	WebUI.click(findTestObject('Object Repository/Page_Medical Language Lab/a_Products'))
}
catch (NoSuchElementException e) {
	System.out.println('An exceptional case.')
}

WebUI.waitForPageLoad(20)

try {
	WebUI.click(findTestObject('Object Repository/Page_Medical Language Lab/a_For Students'))
}
catch (NoSuchElementException e) {
	System.out.println('An exceptional case.')
}

WebUI.waitForPageLoad(20)

try {
	WebUI.click(findTestObject('Object Repository/Page_Medical Language Lab/a_For Instructors'))
}
catch (NoSuchElementException e) {
	System.out.println('An exceptional case.')
}

WebUI.waitForPageLoad(20)

try {
	WebUI.click(findTestObject('Object Repository/Page_Medical Language Lab/a_Support'))
}
catch (NoSuchElementException e) {
	System.out.println('An exceptional case.')
}

WebUI.waitForPageLoad(20)

//WebUI.click(findTestObject('Object Repository/Page_Medical Language Lab/a_Request a Demo'))
//
//WebUI.switchToWindowTitle('Request a Demo of Medical Language Lab - F.A. Davis Company')
//
//WebUI.click(findTestObject('Object Repository/Page_Request a Demo of Medical Language Lab - FA Davis Company/a_Fadaviscom'))
//
//WebUI.click(findTestObject('Object Repository/Page_Textbooks and Classroom Resources for Nursing and Health Professions - FA Davis Company/a_Log in'))

WebUI.closeBrowser()
